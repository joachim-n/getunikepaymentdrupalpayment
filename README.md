README
======

Drupal DDS Getunik Payment Module
-----------------

DDS Getunik Payment Module is a payment type for the Drupal Payment module that integrates the getunik EPayment gateway.

Configuration
------------

The module stores site-wide 5 configuration variables:
'username', 'password', 'apikey', 'test_mode' and 'payment_method_brand'. 
'username' and 'password' is used for server-side api requests. Mostly used for transaction status requests of SMS transactions. 
'test_mode' sets production or test mode of transactions. Test credit cards can be found in the documentation 
'payment_method_brand' defines the payment method brand.


Documentation
-------------

More infos about the getunik EPayment Services are available under 
http://clients.getunik.net/display/DDT/EPIK+-+E-Payment+Integration+Kit
