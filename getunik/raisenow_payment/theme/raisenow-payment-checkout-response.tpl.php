<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div>
            <script type="text/javascript">
                (function ($) {
                    var search;
                    if(location.search.indexOf("epayment_status") > 0) {
                        search = self.parent.Drupal.behaviors.raisenow_payment.getQueryParameters(location.search);
                        self.parent.Drupal.behaviors.raisenow_payment.paymentResponseCallBack(search);
                    }
                })(self.parent.jQuery);
            </script>
        </div>
    </body>
</html>
