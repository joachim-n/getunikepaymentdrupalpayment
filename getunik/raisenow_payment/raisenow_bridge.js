(function ($) {
    Drupal.behaviors.raisenow_payment = {
        attach: function(context, settings) {
            var self = this,
            form,
            button,
            id;
            self.settings = settings.raisenow_payment;
            self.redirectURL = location.protocol + '//' + location.host + '/raisenow-payment/response-callback';
            self.forms = {};

            $("[id^='Drupalraisenow-payment']").each(function () {
                form = $(this).parents('form');
                id = form.attr('id');
                self.forms[id] = form;
                button = form.find('#edit-webform-ajax-submit-' + id);
                if (button.length === 0) { // no webform_ajax.
                    button = form.find('input.form-submit');
                }
                button.unbind('click');
                button.bind('click', self.submitHandler);
            });
        },

        submitHandler: function(event) {
            var parameters,
            self = Drupal.behaviors.raisenow_payment,
            waitingPage,
            iFrame,
            iFrameForm,
            parameter,
            input,
            testMode,
            controller,
            paymentMethod,
            paymentMethodParameters,
            ccControllerIdentifier = 'Drupalraisenow-paymentCreditCardController',
            ezsControllerIdentifier = 'Drupalraisenow-paymentEZSController',
            fieldNamePrefix = "[name='submitted[paymethod_select][payment_method_all_forms][",
            form = $(event.target).parents('form');

            // Remember used form. IMO this should be improved in case of more than one form is processed.
            self.activeForm = form;
            controller = $('#' + form.attr('id') +
                ' .payment-method-form:visible').attr('id'),

            waitingPage = self.redirectURL;

            event.preventDefault();
            event.stopImmediatePropagation();

            iFrame = $("<iframe/>").attr({
                "width": "100%",
                "height": "100%",
                "name" : "epp_iframe",
                "frameborder" : 0,
                "src" : waitingPage
            });


            iFrameForm = $("<form/>").attr({
                "action": self.settings.scheme + self.settings.authority + self.settings.path + self.settings.apikey,
                "method": 'POST'
            });
            testMode = (self.settings['test_mode'] === '1'  || self.settings['test_mode'] == true) ? 'true' : 'false';
            paymentMethod = $(fieldNamePrefix + ccControllerIdentifier + "][issuer]']", form).val();

            parameters = {
                success_url : self.redirectURL,
                error_url : self.redirectURL,
                cancel_url : self.redirectURL,
                test_mode : testMode,
                mobile_mode : "false",
                amount : parseInt($("[name='submitted[amount][donation_amount]']", form).val()) * 100,
                currency : self.settings.currency,
                language : self.settings.language,
                payment_method : paymentMethod
            }

            switch(controller) {
                case ccControllerIdentifier:
                    paymentMethodParameters = {
                            cardno : $(fieldNamePrefix + ccControllerIdentifier + "][credit_card_number]']", form).val(),
                            expy : $(fieldNamePrefix + ccControllerIdentifier + "][expiry_date][year]']", form).val().substring(2),
                            expm : $(fieldNamePrefix + ccControllerIdentifier + "][expiry_date][month]']", form).val(),
                            cvv : $(fieldNamePrefix + ccControllerIdentifier + "][secure_code]']", form).val(),
                            card_holder_name: $("[name='submitted[first_name]']", form).val() + ' ' + $("[name='submitted[last_name]']", form).val(),
                            reqtype : "CAA"
                        };

                    if (!self.validateCreditCard(paymentMethodParameters)) { return; }
                    $.extend(parameters, paymentMethodParameters);
                    break;
                case ezsControllerIdentifier:
                    break;

            }

            for(parameter in parameters) {
                if(parameters.hasOwnProperty(parameter)) {
                    input = $("<input/>").attr({
                        "type": "hidden",
                        "name": parameter,
                        "value": parameters[parameter]
                    });
                    iFrameForm.append(input);
                }
            }

            form.find("iframe[name=epp_iframe]").remove();

            form.append(iFrame);

            if (iFrame[0].readyState) {
                iFrame[0].onreadystatechange = function () {
                    if (this.readyState === "complete" || this.readyState === "loaded") {
                        this.onreadystatechange = null;
                        iFrame.contents().find("body").append(iFrameForm);
                        iFrame.contents().find("body form").submit();
                    }
                };
            } else {
                iFrame[0].onload = function () {
                    this.onload = null;
                    iFrame.contents().find("body").append(iFrameForm);
                    iFrame.contents().find("body form").submit();
                };
            }

            return false;
        },
        paymentResponseCallBack : function(queryParameters) {
            var self = Drupal.behaviors.raisenow_payment,
                input;
            if(self.activeForm) {
                if(queryParameters['epayment_status'] === 'success') {
                    $("[name$='[raisenow_payment_epp_transaction_id]']", self.activeForm).val(queryParameters['epp_transaction_id']);
                    self.activeForm.submit();
                    self.activeForm = undefined;
                } else if(queryParameters['epayment_status'] === 'error' || queryParameters['epayment_status'] === 'cancel') {
                    self.errorHandler('unknown_error');
                }
            }
        },
        getQueryParameters : function(search) {
                var params,
                    queryParams,
                    i,
                    tmp;

                //get the parameters
                search.match(/\?(.+)$/);
                params = RegExp.$1;
                // split up the query string and store in an
                // associative array
                params = params.split("&");
                queryParams = {};

                for(i = 0; i < params.length; i++) {
                    tmp = params[i].split('=');
                    queryParams[tmp[0]] = decodeURIComponent(tmp[1]);
                }
                return queryParams;
        },
        errorHandler: function(error) {
            var self = Drupal.behaviors.raisenow_payment;
            if ($('#messages').length === 0) {
                $('<div id="messages"><div class="section clearfix">' +
                    '</div></div>').insertAfter('#header');
            }
            $('<div class="messages error">' +
                self.settings.error_messages[error] + '</div>')
            .appendTo("#messages .clearfix");

        },

        validateCreditCard: function(p) {
            //TODO validation
            var number = p.cardno;
            if (!number) {
                this.errorHandler('field_invalid_card_number');
            };
            if (!p.expy || !p.expm) {
                this.errorHandler('field_invalid_card_exp');
            };
            if (!p.cvv)    {
                this.errorHandler('field_invalid_card_cvc');
            };

            if (number && p.expm && p.expy && p.cvv) {
                return true;
            }
            else {
                return false;
            };
        }
    };
}(jQuery));


