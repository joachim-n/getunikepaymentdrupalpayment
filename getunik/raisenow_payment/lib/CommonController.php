<?php
/**
 * @file
 *
 * @author    Tsuy Ito <tsuy.ito@getunik.com>
 * @copyright Copyright (c) 2014 copyright
 */
namespace Drupal\raisenow_payment;

class CommonController extends \PaymentMethodController {


   /**
    * The production mode
    */
    const PRODUCTION_MODE = 0;

   /**
    * The testing mode
    */
    const TESTING_MODE = 1;

   /**
    * The test mode
    */
    const TEST_MODE = self::TESTING_MODE;

   /**
    * The server URL.
    */
    const API_PATH = '/epayment/api/step/pay/merchant/';

    const STATUS_API_PATH = '/epayment/api/transaction/status';

    const API_SCHEME = 'https://';

    const API_AUTHORITY = 'dds-pay.getunik.net';


    public $controller_data_defaults = array(
        'username' => '',
        'password' => '',
        'apikey' => '',
        'test_mode' => self::TEST_MODE,
    );

    public function __construct() {
        $this->payment_configuration_form_elements_callback = 'payment_forms_method_form';
        $this->payment_method_configuration_form_elements_callback = '\Drupal\raisenow_payment\configuration_form';
    }

    public function execute(\Payment $payment) {
        $apikey = $payment->method->controller_data['apikey'];
        $username= $payment->method->controller_data['username'];
        $password= $payment->method->controller_data['password'];
        try {
            $eppTransactionId = $payment->method_data['raisenow_payment_epp_transaction_id'];
            $pending = true;
            $counter = 0;
            while($pending) {
                $transaction = json_decode($this->pollTransactionStatus($eppTransactionId, $username, $password, $apikey), true);
                $paymentMethod = strtolower($transaction['payment_method']);
                if($paymentMethod === 'ezs' || $paymentMethod === 'es' || $paymentMethod === 'dd') {
                     $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
                    $pending = false;
                }
                foreach($transaction['status'] as $status) {
                    if($status['statusName'] === 'final_success') {
                         $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
                        $pending = false;
                    } else if($status['statusName'] === 'final_error') {
                        $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_FAILED));
                        $pending = false;
                    } else if($status['statusName'] === 'final_cancel' ||
                            $status['statusName'] === 'aborted_by_user') {
                        $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_CANCELLED));
                        $pending = false;
                    }
                }
                $counter++;
                //transaction should have a final status except in case of ezs, es or dd.
                // Afer 100 seconds payment will set to pending. Furhter action in webhook should be done.
                if($counter > 2) {
                    $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_PENDING));
                    $pending = false;
                }
                if($pending) {
                    sleep(5);
                }
            }

        } catch(\Exception $e) {
            $payment->setStatus(new \PaymentStatusItem(PAYMENT_STATUS_FAILED));
            entity_save('payment', $payment);

            $message = t(
                '@method payment method encountered an error while trying ' .
                'to contact the raisenow server for payment @pid',
                array(
                    '@pid'      => $payment->pid,
                    '@pmid'     => $payment->method->pmid,
                    '@method'   => $payment->method->title_specific,
                ));
            throw new \PaymentException($message);

        }
    }

    private function pollTransactionStatus($eppTransactionId, $username, $password, $apikey) {

        $response = drupal_http_request(CommonController::API_SCHEME . CommonController::API_AUTHORITY . CommonController::STATUS_API_PATH . "?merchant-config=" . $apikey . "&transaction-id=" . $eppTransactionId,
                array('headers' => array("Authorization" => sprintf("Basic %s\r\n", base64_encode($username . ':' . $password))), 'method' => 'GET'));

        return $response->data;
    }

    /**
     * Helper for entity_load().
     */
    public static function load($entities) {
        $pmids = array();
        foreach ($entities as $method) {
            if ($method->controller instanceof CommonController) {
                $pmids[] = $method->pmid;
            }
        }
        if ($pmids) {
            $query = db_select('raisenow_payment_payment_method', 'controller')
              ->fields('controller')
              ->condition('pmid', $pmids);
            $result = $query->execute();
            while ($data = $result->fetchAssoc()) {
                if(!empty($data['payment_method_types'])) {
                    $data['payment_method_types'] = unserialize($data['payment_method_types']);
                }
                $method = $entities[$data['pmid']];
                unset($data['pmid']);
                $method->controller_data = (array) $data;

                $method->controller_data += $method->controller->controller_data_defaults;
            }
        }
    }

    /**
     * Helper for entity_insert().
     */
    public function insert($method) {
        $method->controller_data += $this->controller_data_defaults;

        $query = db_insert('raisenow_payment_payment_method');
        $values = array_merge($method->controller_data, array('pmid' => $method->pmid));
        if(isset($values['payment_method_types'])) {
            $values['payment_method_types'] = serialize($values['payment_method_types']);
        }
        $query->fields($values);
        $query->execute();
    }

    /**
     * Helper for entity_update().
     */
    public function update($method) {
        $query = db_update('raisenow_payment_payment_method');
        $values = array_merge($method->controller_data, array('pmid' => $method->pmid));

        if(isset($values['payment_method_types'])) {
            $values['payment_method_types'] = serialize($values['payment_method_types']);
        }
        $query->fields($values);
        $query->condition('pmid', $method->pmid);
        $query->execute();
    }

    /**
     * Helper for entity_delete().
     */
    public function delete($method) {
        db_delete('paymill_payment_payment_method_controller')
          ->condition('pmid', $method->pmid)
          ->execute();
    }

    public function configurationForm(array $form, array &$form_state) {

        $controller_data = $form_state['payment_method']->controller_data + $this->controller_data_defaults;

        $form['username'] = array(
            '#default_value' => $controller_data['username'],
            '#description' => t('The username.'),
            '#required' => FALSE,
            '#title' => t('Username'),
            '#type' => 'textfield',
        );
        $form['password'] = array(
            '#default_value' => $controller_data['password'],
            '#description' => t('The password.'),
            '#required' => FALSE,
            '#title' => t('Password'),
            '#type' => 'textfield',
        );
        $form['apikey'] = array(
            '#default_value' => $controller_data['apikey'],
            '#description' => t('The api key'),
            '#required' => TRUE,
            '#title' => t('Api key'),
            '#type' => 'textfield',
        );
        $form['test_mode'] = array(
            '#default_value' => $controller_data['test_mode'],
            '#options' => array(
                CommonController::PRODUCTION_MODE => ('Production'),
                CommonController::TESTING_MODE => ('Testing'),
            ),
            '#required' => TRUE,
            '#title' => t('Test mode'),
            '#type' => 'radios',
        );

        return $form;
      }

    public function validateForm(array $element, array &$form_state) {
        $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
        $form_state['payment_method']->controller_data['apikey'] = $values['apikey'];
        $form_state['payment_method']->controller_data['username'] = $values['username'];
        $form_state['payment_method']->controller_data['password'] = $values['password'];
        $form_state['payment_method']->controller_data['test_mode'] = $values['test_mode'];
        $form_state['payment_method']->controller_data['payment_method_types'] = $values['payment_method_types'];
    }

}

/* Implements PaymentMethodController::payment_method_configuration_form_elements_callback().
 *
 * @return array
 *   A Drupal form.
 */
function configuration_form(array $form, array &$form_state) {

    return $form_state['payment_method']->controller->configurationForm($form, $form_state);
}

/**
 * Implements form validate callback for
 * \paymill_payment\configuration_form().
 */
function configuration_form_validate(array $element, array &$form_state) {

    return $form_state['payment_method']->controller->validateForm($element, $form_state);
}
