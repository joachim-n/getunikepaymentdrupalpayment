<?php
/**
 * @file
 *
 * @author    Tsuy Ito <tsuy.ito@getunik.com>
 * @copyright Copyright (c) 2014 copyright
 */

namespace Drupal\raisenow_payment;

class CommonForm {
  public static function getSettings($payment) {
    global $language_content;
    $method = $payment->method;
    return array(
      'raisenow_payment' => array(
            'apikey' => $method->controller_data['apikey'],
            'authority' => CommonController::API_AUTHORITY,
            'path' => CommonController::API_PATH,
            'scheme' => CommonController::API_SCHEME,
            'test_mode' => $payment->method->controller_data['test_mode'],
            'payment_method_types' => $payment->method->controller_data['payment_method_types'],
            'language' => $language_content->language,
            'currency' => $payment->currency_code,
            'error_messages' => array(
                'internal_server_error'        => t('Internal server error'),
                'invalid_public_key'           => t('Invalid public key'),
                'invalid_payment_data'         => t('Invalid payment data'),
                'unknown_error'                => t('Unknown error'),
                '3ds_cancelled'                => t('3-D Secure cancelled'),
                'field_invalid_card_number'    => t('Invalid card number'),
                'field_invalid_card_exp_year'  => t('Invalid expiry year'),
                'field_invalid_card_exp_month' => t('Invalid expiry month'),
                'field_invalid_card_exp'       => t('Invalid expiry date.'),
                'field_invalid_card_cvc'       => t('Invalid CVC'),
                'field_invalid_card_holder'    => t('Invalid card holder'),
                'field_invalid_amount_int'     => t('Invalid amount'),
                'field_invalid_currency'       => t('Invalid currency'),
                'field_invalid_account_number' => t('Invalid account_number'),
                'field_invalid_account_holder' => t('Invalid account_holder'),
                'field_invalid_bank_code'      => t('Invalid bank code'),
                'field_invalid_iban'           => t('Invalid bic'),
                'field_invalid_bic'            => t('Invalid bic'),
                'field_invalid_country'        => t('Invalid country'),
                'field_invalid_bank_data'      => t('Invalid bank data'),
            )
      ));
  }

    public static function addRaiseNowBridge(&$form) {
        $form['#attached']['js'] = array(
          array(
            'data' => drupal_get_path('module', 'raisenow_payment') .
              '/raisenow_bridge.js',
            'type' => 'file'
          ));
    }

    public static function addEppTransactionIdField(&$form) {
        $form['raisenow_payment_epp_transaction_id'] = array(
            '#type' => 'hidden',
            '#attributes' => array('class' => array()),
            );
    }

    public static function addEppTransactionIdToPaymentMethodData(&$element, &$form_state) {
        $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
        $form_state['payment']->method_data['raisenow_payment_epp_transaction_id'] = $values['raisenow_payment_epp_transaction_id'];
    }
}
